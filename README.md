# Python Rest API template

Try it yourself:

1. Install `cookiecutter` if it is not already installed on your machine

```bash
pip install cookiecutter
```

2. Create a new project based on this template:

```bash
cookiecutter https://gitlab.com/gu.charbon/python-rest-api.git
```
