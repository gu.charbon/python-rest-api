import glob
from setuptools import setup, find_packages


def readme():
    """
    Return content of README file as string
    """
    if glob.glob("README.md"):
        with open('README.md') as readmefile:
            return readmefile.read()
    else:
        return ""


setup(
    name="{{ cookiecutter.package_name }}",
    version="{{ cookiecutter.version }}",
    description="{{ cookiecutter.description }}",
    long_description=readme(),
    classifiers=[
        "Programming Language :: Python :: 3.6"
        "Programming Language :: Python :: 3.7"
    ],
    url="{{ cookiecutter.url }}",
    author="{{ cookiecutter.author }}",
    author_email="{{ cookiecutter.author_email }}",
    license="{{ cookiecutter.license }}",
    packages=find_packages(
        exclude=["tests"]
    ),
    install_requires=[
        "falcon",
        "gunicorn"
    ],
    extras_require={
        "dev": [
            # Python code formatter
            "black",
            # Test suite
            "pytest",
            "pytest-cov",
            # Code Linter
            "flake8",
            # Security Linter
            "bandit"
        ],
        "doc": [
            "mkdocs",
            "mkdocs-material",
            "pymdown-extensions"
        ]
    },
    zip_safe=False,
    entry_points={
        'console_scripts': [],
    },
    include_package_data=True
)
