"""
Test {{ cookiecutter.package_name }} using pytest
"""
from falcon import testing
import pytest

from {{ cookiecutter.package_name }}.app import create
from {{ cookiecutter.package_name }}.static import OPENAPI_SPECIFICATION


@pytest.fixture()
def client():
    """
    Initialize a test client using the create function from lca_rest_api
    """
    return testing.TestClient(create())


def test_get_version(client):
    doc = {"version": "1.0.0"}

    result = client.simulate_get("/version")
    assert result.json == doc
    assert result.status == "200 OK"
    assert result.status_code == 200


def test_get_openapi(client):
    with open(OPENAPI_SPECIFICATION, "r") as f:
        body = f.read()

    result = client.simulate_get("/openapi")
    assert result.text == body
    assert result.status == "200 OK"
    assert result.status_code == 200
