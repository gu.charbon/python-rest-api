"""
The falcon application is defined in this file.
"""
import falcon
from .routes import Version, OpenAPIv3


def create():
    """
    initialize and return a `falcon.API` instance.
    """
    app = falcon.API()

    app.add_route("/version", Version())
    app.add_route("/openapi", OpenAPIv3())

    return app


app = create()
