"""
The routes module defines all the endpoints of the REST API.
"""
from .version import Version
from .openapi import OpenAPIv3
