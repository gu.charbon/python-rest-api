"""
This endpoint returns the OpenAPI v3 specification of the REST API
"""
import falcon
from ..static import OPENAPI_SPECIFICATION


class OpenAPIv3:
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200
        resp.content_type = "application/yaml"
        with open(OPENAPI_SPECIFICATION, "r") as f:
            resp.body = f.read()
