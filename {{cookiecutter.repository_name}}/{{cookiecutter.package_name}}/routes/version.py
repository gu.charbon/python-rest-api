"""
This endpoint returns the version of the Rest API
"""
import falcon
from ..version import __version__


class Version:
    def on_get(self, req, resp):
        """Handles GET requests"""
        resp.status = falcon.HTTP_200
        msg = {"version": __version__}
        resp.media = msg
