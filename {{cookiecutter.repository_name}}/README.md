# {{cookiecutter.repository_name}}

> What to put here ?

> One Paragraph of project description goes here.


* [Getting started](#getting_started)
  * [Prerequisites](#prerequisites)
  * [Installing](#installing)
  * [Running the tests](#tests)
  * [Coding style tests](#style_tests)
* [Contributing](#contributing)
* [Versionning](#versionning)
* [Authors](#authors)
* [License](#license)
* [Acknowledgments](#acknowledgments)


## Getting Started
<a name="getting_started"></a>

> What to put here ? These instructions should get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
<a name="prerequisites"></a>

> What to find here ?  Mention steps that should be necessary before installing the package.


### Installing
<a name="installing"></a>

> What to find here ? Instructions about installation of repository.


### Running the tests
<a name="tests"></a>

Unit tests are written using [pytest](https://docs.pytest.org/en/latest/) framework.

All unit tests are available in folder [`tests`](tests/)

TO run the test, simply execute the following command:

```bash
python setup.py test
```

### Coding style tests
<a name="style_tests"></a>

We use [pylint](https://www.pylint.org) to check that coding style is OK. From the root of the repository simply run:

By default coding style tests are ran when executing `python setup.py test`


## Contributing
<a name="contributing"></a>

Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.


## Versioning
<a name="versioning"></a>

> What to find here ? Add some information about semantic versioning used. See [semver](https://semver.org) for more info.


## Authors
<a name="authors"></a>

**Author**: [{{ cookiecutter.author }}](mailto:{{ cookiecutter.author_email }})


## License
<a name="license"></a>

> What to find here ? You should add some information about the license of the project


## Acknowledgments
<a name="acknowledgments"></a>

* Hat tip to anyone whose code was used
* Inspiration
* etc
