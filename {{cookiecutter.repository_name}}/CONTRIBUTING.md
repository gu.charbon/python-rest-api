# Contributing to {{ cookiecutter.package_name }}

Contributions are welcome, and they are greatly appreciated! Every
little bit helps, and credit will always be given.

## Got a Question or Problem?

Do not open issues for general support questions as we want to keep {{ cookiecutter.package_name }} issues for bug reports and feature requests. You've got much better chances of getting your question answered on dedicated support platforms, the best being Stack Overflow.

To save your and our time, we will systematically close all issues that are requests for general support and redirect people to the section you are reading right now.

Contact the owner of repository for more information regarding support.

## Found an Issue or a Bug ?

If you find a bug in the source code, you can help us by submitting an issue to our GitLab Repository. Even better, you can submit a Pull Request with a fix.

Please note we have a code of conduct, try to follow it in all your interactions with the project.

## Issue Submission Guidelines:

Before you submit your issue search the archive, maybe your question was already answered.

If your issue appears to be a bug, and hasn't been reported, open a new issue. Help us to maximize the effort we can spend fixing issues and adding new features, by not reporting duplicate issues.

In general, providing the following information will increase the chances of your issue being dealt with quickly:

- **Overview of the issue**: if an error is being thrown a non-minified stack trace helps

- **Motivation for or use case**: Explain why this is a bug for you

- **{{ cookiecutter.package_name }} version**: Is it a regression?

- **Reproduce the error**: provide an unambiguous set of steps to reproduce the error

- **Suggest a fix**: If you can't fix the bug yourself, perhaps you can point to what might be causing the problem. (commit ref or line of code for examples)

## Pull Request Submission Guidelines

Before you submit your pull request consider the following guidelines:

- Search on GitLab for an open or closed merge Request that relates to your submission. You don't want to duplicate effort.

- If no pull request exists create the merge request from GitLab UI

- Create a new branch  associated with the merge request and start making your changes

- Create your patch commit, including appropriate test cases

- Run the pylint and py.test test suites and ensure all pass.

- If the changes affect public APIs, change or add relevant documentation.

- Push the commit to the new remote branch and wait for CI results

- If we suggest changes, then:
  - Make the required updates.
  - Push updates to trigger pipeline until all tests pass

That's it! Thank you for your contribution!

> Note: After your pull request is merged, you can safely delete your branch and pull the changes from the main (upstream) repository.
