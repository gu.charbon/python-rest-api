"""
Post hook script for cookiecutter-pf
"""

print("""                                                                                          
,------.       ,--.                         ,------. ,------. ,---. ,--------.      ,---.  ,------. ,--. 
|  .---',--,--.|  | ,---. ,---. ,--,--,     |  .--. '|  .---''   .-''--.  .--'     /  O  \ |  .--. '|  | 
|  `--,' ,-.  ||  || .--'| .-. ||      \    |  '--'.'|  `--, `.  `-.   |  |       |  .-.  ||  '--' ||  | 
|  |`  \ '-'  ||  |\ `--.' '-' '|  ||  |    |  |\  \ |  `---..-'    |  |  |       |  | |  ||  | --' |  | 
`--'    `--`--'`--' `---' `---' `--''--'    `--' '--'`------'`-----'   `--'       `--' `--'`--'     `--'                                                                                                 
""")

hash_line = "*********" * 3

print(
"""
Start your API running the following command:

$ cd {{ cookiecutter.repository_name }} 
$ make serve

Visit http://localhost/version to check that everything is working :)
"""
)
